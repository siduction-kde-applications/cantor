Source: cantor
Section: kde
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Sune Vuorela <sune@debian.org>, Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake (>= 2.8.12~),
               debhelper (>= 9),
               extra-cmake-modules (>= 1.1.0~),
               kde-sc-dev-latest (>= 4:4.12),
               kdelibs5-dev,
               libblas3,
               libglib2.0-dev,
               libkf5archive-dev,
               libkf5config-dev,
               libkf5coreaddons-dev,
               libkf5kdelibs4support-dev,
               libkf5newstuff-dev,
               libkf5parts-dev,
               libkf5pty-dev,
               libkf5texteditor-dev,
               liblapack3,
               libluajit-5.1-dev [i386 amd64],
               libqalculate-dev,
               libqt5svg5-dev (>= 5.4),
               libqt5xmlpatterns5-dev (>= 5.4),
               libspectre-dev,
               pkg-config,
               pkg-kde-tools (>= 0.12),
               python2.7-dev,
               qtbase5-dev (>= 5.4),
               r-base-core
Standards-Version: 3.9.6
XS-Testsuite: autopkgtest
Homepage: http://edu.kde.org/
Vcs-Browser: http://anonscm.debian.org/cgit/pkg-kde/applications/cantor.git
Vcs-Git: git://anonscm.debian.org/pkg-kde/applications/cantor.git

Package: cantor
Architecture: any
Section: math
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: cantor-backend-qalculate, texlive-binaries, texlive-latex-base
Suggests: cantor-backend-lua [i386 amd64],
          cantor-backend-maxima,
          cantor-backend-octave,
          cantor-backend-python2,
          cantor-backend-r,
          cantor-backend-sage,
          cantor-backend-scilab [i386 amd64]
Description: interface for mathematical applications
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 Cantor supports various mathematical applications as backends (provided in
 external packages):
  * Maxima Computer Algebra System (cantor-backend-maxima)
  * R Project for Statistical Computing (cantor-backend-r)
  * Sage Mathematics Software (cantor-backend-sage)
  * Octave (cantor-backend-octave)
  * Python (cantor-backend-python2)
  * Scilab (cantor-backend-scilab)
  * Qalculate! (cantor-backend-qalculate)
  * Lua (cantor-backend-lua)
 .
 This package is part of the KDE education module.

Package: cantor-backend-maxima
Architecture: any
Section: math
Depends: maxima, ${misc:Depends}, ${shlibs:Depends}
Description: Maxima backend for Cantor
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the backend for using the Maxima Computer Algebra System
 (http://maxima.sourceforge.net) in Cantor.
 .
 This package is part of the KDE education module.

Package: cantor-backend-octave
Architecture: any
Section: math
Depends: octave, ${misc:Depends}, ${shlibs:Depends}
Description: Octave backend for Cantor
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the backend for using the GNU Octave language for
 numerical computations (http://www.octave.org/) in Cantor.
 .
 This package is part of the KDE education module.

Package: cantor-backend-python2
Architecture: any
Section: math
Breaks: cantor-backend-python (<< 4:4.13.0-0ubuntu2~)
Replaces: cantor-backend-python (<< 4:4.13.0-0ubuntu2~)
Depends: python, ${misc:Depends}, ${shlibs:Depends}
Description: Python backend for Cantor
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the backend for using the Python language
 (http://www.python.org/) in Cantor.
 .
 This package is part of the KDE education module.

Package: cantor-backend-qalculate
Architecture: any
Section: math
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Qalculate! backend for Cantor
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the backend for using the Qalculate! calculator
 (http://qalculate.sourceforge.net/) in Cantor.
 .
 This package is part of the KDE education module.

Package: cantor-backend-r
Architecture: any
Section: math
Depends: r-base-core, ${misc:Depends}, ${shlibs:Depends}
Suggests: r-base-html
Description: R backend for Cantor
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the backend for using the R Project for Statistical
 Computing (http://www.r-project.org) in Cantor.
 .
 This package is part of the KDE education module.

Package: cantor-backend-sage
Architecture: any
Section: math
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: sagemath
Description: Sage backend for Cantor
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the backend for using the Sage Mathematics Software
 (http://www.sagemath.org) in Cantor.
 .
 Please note that sagemath is not available in Debian, so you need to manually
 install it for this package to work.
 .
 This package is part of the KDE education module.

Package: cantor-backend-scilab
Architecture: amd64 i386
Section: math
Depends: scilab-cli, ${misc:Depends}, ${shlibs:Depends}
Description: Scilab backend for Cantor
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the backend for using the Scilab scientific software
 package for numerical computations (http://www.scilab.org) in Cantor.
 .
 This package is part of the KDE education module.

Package: cantor-backend-lua
Architecture: amd64 i386
Section: math
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Lua backend for Cantor
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the backend for using the Lua language
 (http://www.lua.org) in Cantor.
 .
 This package is part of the KDE education module.

Package: cantor-dbg
Architecture: any
Section: debug
Priority: extra
Depends: cantor (= ${binary:Version}), ${misc:Depends}
Breaks: kdeedu-dbg (<< 4:4.6.80)
Replaces: kdeedu-dbg (<< 4:4.6.80)
Description: debugging symbols for cantor
 This package contains debugging files used to investigate problems with
 binaries included in cantor.
 .
 This package is part of the KDE education module.

Package: cantor-backend-python
Architecture: all
Priority: extra
Section: oldlibs
Depends: cantor-backend-python2
Description: Python backend for Cantor -- transitional package
 Transitional package for cantor-backend-python2
 .
 This package can be safely removed after installation
